# Reportes

Este Proyecto fue generado con [Angular CLI](https://github.com/angular/angular-cli) version 1.6.3.

## Node_modules

Ejecutar `npm install` para instalar los modulos necesarios.

## Development server

Ejecutar `npm start` para iniciar el servidor en modo dev mediante WebPack. Navegar a `http://localhost:4200/`. La aplicacion se ejecutara pudiendose observar el  Document Viewer de DevExpress

## Bind BackEnd 

En caso de ser necesario modificar el host por la url del proyecto backend

```ts
constructor(private renderer: Renderer2) { }

  ngAfterViewInit() {
    const reportUrl = this.koReportUrl,
      host = 'http://localhost:55041/', // Remplazar por la URL y puerto en el que se ejecute tu servicio backend
```